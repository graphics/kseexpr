# Pregenerated parser files

This directory contains pregenerated parser files for use in Windows-based 
builds, or anywhere where there is no Bison/FLEX provided by default).

These files are automatically copied by CMake, no action is needed by the user.
